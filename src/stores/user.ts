import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import userService from '@/services/user'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {
  const loadingStore = useLoadingStore()
  const users = ref<User[]>([])

  async function getUser(id: number) {
    loadingStore.doLoad()
    const res = await userService.getUser(id)
    users.value = res.data
    loadingStore.finish()
  }
  //Data function

  async function getUsers() {
    loadingStore.doLoad()
    const res = await userService.getUsers()
    users.value = res.data
    loadingStore.finish()
  }
  async function saveUser(user: User) {
    loadingStore.doLoad()
    if (user.id < 0) {
      //Add new
      console.log('Post' + JSON.stringify(user))
      const res = await userService.addUser(user)
    } else {
      // Update
      console.log('Patch' + JSON.stringify(user))
      const res = await userService.updateUser(user)
    }
    await getUsers()
    loadingStore.finish()
  }

  async function deleteUser(user: User) {
    loadingStore.doLoad()
    const res = await userService.delUser(user)
    await getUsers()
    loadingStore.finish()
  }
  return { users, getUsers, saveUser, deleteUser ,getUser}
})
